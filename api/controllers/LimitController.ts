class LimitController{
  async create(req: Request, res: Response) {
    const rules = {
      profile_name: 'required',
      daily_send_amount: 'required|integer',
      daily_receive_amount: 'required|integer',
      daily_send_count: 'required|integer',
      daily_receive_count: 'required|integer'
    };
    let limit:Limit;
    limit = {
      profile_name: req.body.profile_name,
      daily_send_amount: parseInt(req.body.daily_send_amount),
      daily_receive_amount: parseInt(req.body.daily_receive_amount),
      daily_send_count: parseInt(req.body.daily_send_count),
      daily_receive_count: parseInt(req.body.daily_receive_count)
    };
    try{
      var result = await CrudService.create(Limit,rules,limit);
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).send(error.error);
    }
  }
  async getById(req: Request, res: Response) {
    try{
      var result = await CrudService.getById(Limit, parseInt(req.params.id));
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).send(error.error);
    }
  }
  async getall(req: Request, res: Response) {
    try{
      var result = await CrudService.getall(Limit);
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).send(error.error);
    }
  }
  async update(req: Request, res: Response) {
    try {
      let result = await CrudService.update(Limit,parseInt(req.params.id),req.body);
      res.status(200).json({newRow : result});
    } catch (error) {
      res.status(error.status).json({error:error});
    }
  }
  async delete(req: Request, res: Response) {
    try{
      var result = await CrudService.delete(Limit,parseInt(req.params.id));
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).send(error.error);
    }
  }

}
module.exports = new LimitController();
