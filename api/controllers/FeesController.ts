export{}
const { validateAll } = require('indicative');

class FeesController{
  async create(req: Request, res: Response) {
    const rules = {
      profile_name: 'required|string',
      sender_fees: 'required|integer',
      receive_fees: 'required|integer',
      s_fees_type: 'required',
      r_fees_type: 'required'
    };
    let fees:Fees;
    fees = {
      profile_name:req.body.profile_name,
      sender_fees:parseInt(req.body.sender_fees),
      receive_fees:parseInt(req.body.receive_fees),
      s_fees_type: req.body.s_fees_type,
      r_fees_type:req.body.r_fees_type
    };
    try{
      var result = await CrudService.create(Fees,rules,fees);
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).send(error.error);
    }

    // let crud = new CRUD(Fees);
    // await crud.create(req,res,rules,fees);
  }
  async getById(req: Request, res: Response) {
    try {
      var result = await CrudService.getById(Fees,parseInt(req.params.id));
      res.status(200).json(result);
    } catch (error) {
      res.status(error.status).send(error.error);
    }
  }
  async getall(req: Request, res: Response) {
    try {
      var result = await CrudService.getall(Fees);
      res.status(200).json(result);
    } catch (error) {
      res.status(error.status).send(error.error);
    }
  }
  async update(req: Request, res: Response) {
    try {
      let result = await CrudService.update(Fees,parseInt(req.params.id),req.body);
      res.status(200).json({newRow : result});
    } catch (error) {
      res.status(error.status).json({error:error});
    }
  }
  async delete(req: Request, res: Response) {
    try {
      var result = await CrudService.delete(Fees,parseInt(req.params.id));
      res.status(200).json(result);
    } catch (error) {
      res.status(error.status).send(error.error);
    }
  }

}
module.exports = new FeesController();
