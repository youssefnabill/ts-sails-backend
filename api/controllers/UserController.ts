export{}
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { validateAll } = require('indicative');
class UserController {

  async create(req: Request, res: Response) {
    const rules = {
      email: 'required|email',
      password: 'required',
      name: 'required',
      username: 'required',
      phone_number: 'required',
      limit_id: 'required',
      fees_id: 'required',
    };
    let user:User;
    let hash = await bcrypt.hash(req.body.password, 10);
    user = {
      id: null,
      username: req.body.username,
      name: req.body.name,
      email: req.body.email,
      password: hash,
      balance: 0,
      phone_number: req.body.phone_number,
      fees_id: req.body.fees_id,
      limit_id: req.body.fees_id,
      day_sent: 0,
      day_sent_count: 0,
      day_received: 0,
      day_received_count: 0,
      month_receive: 0,
      month_sent: 0,
      type: 'p',
    };
    try{
      var result = await CrudService.create(User,rules,user);
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).send(error.error);
    }
  }
  async getById(req: Request, res: Response) {
    try{
      var result = await CrudService.getById(User,parseInt(req.params.id));
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).send(error.error);
    }
  }
  async getall(req: Request, res: Response) {
    try{
      var result = await CrudService.getall(User);
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).send(error.error);
    }
  }
  async update(req: Request, res: Response) {
    try {
      if(req.body.password){
        req.body.password = await bcrypt.hash(req.body.password,10);
      }
      let result = await CrudService.update(User,req.params.id,req.body);
      res.status(200).json({newRow : result});
    } catch (error) {
      res.status(error.status).json({error:error});
    }
  }
  async delete(req: Request, res: Response) {
    try{
      var result = await CrudService.delete(User,parseInt(req.params.id));
      res.status(200).json({res: result});
    }catch(error){
      res.status(error.status).send(error.error);
    }
  }

  async login(req:Request, res:Response){
    let email = req.body.email;
    let password = req.body.password;
    //setting the rules of validation
    const rules = {
      email: 'required|email',
      password: 'required|string',
    };
    //validating data from the request
    try{
      await validateAll({
        email : email,
        password: password
      },rules);
    }catch(error){
      return res.status(500).json({error: error});
    }

    //getting the user by email
    try {
      let user = await User.findOne({
        where: {
          email : email
        }
      });
      //comparing user password with the input password
      let result = await bcrypt.compare(password,user.password);
      if(result){
        //password match and sending jwt in response
        var token = jwt.sign({
          id: user.id,
          isAdmin: false,
          isSuper: false
        },'secret');
        res.status(200).json({
          token: token,
          id: user.id,
          isAdmin: false,
          isSuper: false
        });
      }else{
        res.status(500).json({error:"password doesn't match"});
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({error: error});
    }
  }

  async getUserByEmail(req: Request, res: Response){
      try {
        const user = await User.findOne({
          where: {
            email : req.body.email
          }
        });
        res.status(200).json(user);
      } catch (error) {
        res.status(500).send("user not found");
      }
  }

}

module.exports = new UserController();
