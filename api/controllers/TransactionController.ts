import { OperationContext } from "./Transaction/OperationContext";
import { SendOperation } from "./Transaction/SendOperation";
import { receiveOperation } from "./Transaction/receiveOperation";

export{}
const Op = require('sequelize').Op;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { validateAll } = require('indicative');
class TransactionController {
  /*
    params:{
      senderId : the id of the sender
      receiverId: the user id of the receiver
      amount: the amount of money that needed to me transfared
    }
  */
  async makeTransaction(req:Request, res:Response){
    const rules = {
      sender_id : 'required',
      receiver_id: 'required',
      amount:'required',
      senderType: 'required',
      receiverType: 'required'
    };
    try {
      await validateAll(req.body, rules);
    } catch (error) {
      res.status(400).json(error);
      return;
    }
    //getting the sender user data from db
    try{
      let sendOp =  new OperationContext(new SendOperation(parseInt(req.body.sender_id),parseInt(req.body.receiver_id),parseInt(req.body.amount), req.body.senderType, req.body.receiverType));
      let newTransaction:any = await sendOp.executeOperation();

      let receiveOp = new OperationContext(new receiveOperation(parseInt(req.body.receiver_id),parseInt(newTransaction.id)));
      await receiveOp.executeOperation();

      res.status(200).json({ok:true});
    }catch(error){
      console.log('error',error);
      res.status(500).send(error);
    }

  }

  async getTransactionsByUser(req: Request, res: Response){
    try {
      const id = parseInt(req.params.id);
      const rules:Object = {
        id : 'required|number'
      };
      await validateAll(id, rules);
      const transactions = await Transaction.findAll({
        where : {
        [Op.or]:[
         { 'sender_id' : id },
         { 'receiver_id' : id },
        ]
        },
          include: [
            {model:User, as:'sender'},
            {model:User, as:'receiver'}
          ]
      });
      res.status(200).json(transactions);
    } catch (error) {
      res.status(500).json({error: error});
      console.log(error);
    }
  }

  async getAllTransactions(req: Request, res: Response){
    try{
      const trxs = await CrudService.getall(Transaction);
      res.status(200).json(trxs);
    }catch(error) {
      res.status(500).json({error: error});
    }
  }

  async getById(req: Request, res: Response) {
    try{
      var result = await CrudService.getById(Transaction,parseInt(req.params.id));
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).send(error.error);
    }
  }

  async test(req: Request, res: Response){
    console.log("validating user input");
    console.log("setting rules");
    //validating data
    const rules = {
      sender_id : 'required',
      receiver_id: 'required',
      amount:'required'
    }
    try {
      console.log("trying to validate");
      await validateAll(req.body, rules);
      console.log("validation passed");
    } catch (error) {
      res.status(400).json({error:error});
      return;
    }
    console.log("setting amount variable");
    let amountToSend = req.body.amount;
    //getting the sender user data from db

    console.log("getting user info from db");
    let senderUser: User;
    try {
      senderUser = await CrudService.getById(User,parseInt(req.body.sender_id));
      console.log("info retreived");
    } catch (error) {
      res.status(error.status).json(error.error);
      return;
    }
    //check if he has suffient amount to send
    if(senderUser === null){
      res.status(400).send("sender user is null");
    }
    if(senderUser.balance < amountToSend){
      res.status(400).send("insuffient amount. Transaction Refused");
      return;
    }
    //gettng the sender limits and fees
    try {
      var senderLimit:Limit = await CrudService.getById(Limit,senderUser.limit_id);
      var senderFees:Fees = await CrudService.getById(Fees,senderUser.fees_id);
    } catch (error) {
      res.status(error.status).json(error.error);
      return;
    }

    //calculating the sending fees
    var sendingFee:number = 0;
    if(senderFees.s_fees_type == "flat"){
       sendingFee = senderFees.sender_fees;
    }else{
      sendingFee = senderFees.sender_fees/100*parseInt(amountToSend);
    }
    console.log("day_sent: "+ senderUser.day_sent);
    console.log(amountToSend);
    console.log(sendingFee);

    console.log(senderUser.day_sent + parseInt(amountToSend)+ sendingFee);
    //check if the user exceeded the daily amount he can send

    if(senderUser.day_sent + parseInt(amountToSend)+ sendingFee > senderLimit.daily_send_amount){
      res.status(400).send("you have exceeded the daily amount to send. Transaction Refused");
      return;
    }
    //check if the user exceeded the number of daily count he can send
    //----------------------
    //unimplemented check
      if(senderUser.day_sent_count+1 > senderLimit.daily_send_count){
        res.status(400).send("you have exceeded the daily count to send. Transaction Refused");
        return;
      }
    //---------------------

    //the sender passed the tests and is ready to make the transaction

    //get all receiver data from db
    try {
      var receiverUser:User = await CrudService.getById(User,parseInt(req.body.receiver_id));
      var receiverLimit:Limit = await CrudService.getById(Limit,receiverUser.limit_id);
      var receiveFees:Fees = await CrudService.getById(Fees, receiverUser.fees_id);
    } catch (error) {
      res.status(error.status).json(error.error);
      return;
    }

    //calculating the receiving fees
    var receivingFee = 0;
    if(receiveFees.s_fees_type == "flat"){
       receivingFee = receiveFees.receive_fees;
    }else{
      receivingFee = receiveFees.receive_fees/100*parseInt(amountToSend);
    }

    console.log(receiverUser.day_received+ parseInt(amountToSend) +receivingFee);

    //check if the receiver has exceeded the daily amount he can receive
    if(receiverUser.day_received+ parseInt(amountToSend) +receivingFee >= receiverLimit.daily_receive_amount){
      res.status(400).send("the receiver have exceeded the daily amount to receive. Transaction Refused");
      return;
    }

    //check if the receiver exceeded the number of daily count he can receive
    //----------------------
    //unimplemented check
    // console.log(receiverUser.day_received_count +1);
    // console.log(receiverLimit.daily_receive_count);

      if(receiverUser.day_received_count +1 > receiverLimit.daily_receive_count){
        res.status(400).send("the receiver have exceeded the daily count to receive. Transaction Refused");
        return;
      }
    //---------------------

    //receiver has passed the tests and ready to receive the transaction

    //creating new transaction
    let newTransaction:Transaction = {
      amount: amountToSend,
      date: null,
      sender_id: senderUser.id,
      receiver_id: receiverUser.id,
      sender_balance_before: senderUser.balance,
      sender_balance_after: senderUser.balance - (parseInt(amountToSend) + sendingFee),
      sender_fees: sendingFee,
      receiver_balance_before: receiverUser.balance,
      receiver_balance_after: receiverUser.balance + (parseInt(amountToSend) - receivingFee),
      receiver_fees: receivingFee,
      type: 'p2p'
    };
    try {
      var trx = await CrudService.create(Transaction,{},newTransaction);
    } catch (error) {
      res.status(error.status).json(error.error);
      return;
    }

    //updating new sender and receiver data

    const new_day_sent_count = senderUser.day_sent_count+1;
    const new_day_received_count = receiverUser.day_received_count+1;

    let senderNewData = {
      balance: senderUser.balance - (parseInt(amountToSend) + sendingFee),
      day_sent: senderUser.day_sent + (parseInt(amountToSend) + sendingFee),
      month_sent: senderUser.month_sent + (parseInt(amountToSend) + sendingFee),
      day_sent_count: new_day_sent_count
    };
    let receiverNewDate = {
      balance: receiverUser.balance + parseInt(amountToSend) - receivingFee,
      day_received: receiverUser.day_received + (parseInt(amountToSend) - receivingFee),
      month_receive: receiverUser.month_receive + (parseInt(amountToSend) - receivingFee),
      day_received_count : new_day_received_count
    };
    try {
      await CrudService.update(User,senderUser.id, senderNewData);
      await CrudService.update(User,receiverUser.id, receiverNewDate);
      res.status(200).json({res: "transaction completed"});
    } catch (error) {
      await CrudService.delete(Transaction,trx.id);
      res.status(500).json({error:error});
      return;
    }


  }

}

module.exports = new  TransactionController();
