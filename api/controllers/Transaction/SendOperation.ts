import { IOperation } from "./IOperation";
import { TypeFactory } from "./transactionTypes/TypeFactory";
import { ITrxType } from "./transactionTypes/ITrxType";


export class SendOperation implements IOperation{
    senderId:number;
    amountToSend:number;
    receiverId:number;
    senderType:string;
    receiverType:string

    constructor(senderId:number,receiverId:number, amountToSend:number, senderType:string, receiverType:string){
      this.senderId = senderId;
      this.amountToSend = amountToSend;
      this.receiverId = receiverId;
      this.senderType = senderType;
      this.receiverType = receiverType;
    }
    async execute () {

      let transactionTypeFactory = new TypeFactory();
      let transactionType: ITrxType = transactionTypeFactory.getType(this.senderType+'2'+this.receiverType);
      try{
        let newTransaction = await transactionType.send(this.senderId, this.receiverId, this.amountToSend);
        return newTransaction;
      }catch(error){
        console.log(error);
        throw error;
      }

    }
}
