import { IOperation } from "./IOperation";

export class OperationContext{
  operation: IOperation;

  constructor(operation:IOperation){
    this.operation = operation;
  }
  executeOperation():any{
    return this.operation.execute();
  }
}
