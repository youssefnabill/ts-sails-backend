import { ITrxType } from "./ITrxType";

export class p2m implements ITrxType{
  senderUser:any;
  receiverUser:any;
  amountToSend:number;
  transaction:any;

  //local variables
  senderFees:Fees;
  senderLimit:Limit;
  sendingFee:number = 0;
  receivingFee:number = 0;
  receiverLimit:Limit;
  receiverFees:Fees;


  init(senderUser:any, receiverUser:any, amountToSend:number, transaction:any):any{
    this.senderUser = senderUser;
    this.receiverUser = receiverUser;
    this.amountToSend = amountToSend;
    this.transaction = transaction;
  }

  async send() {
    try {
      await this.validateSending();
    } catch (error) {
      throw error;
    }

    let newTransaction:Transaction = {
      amount: this.amountToSend,
      date: null,
      sender_id: this.senderUser.id,
      receiver_id: this.receiverUser.id,
      sender_balance_before: this.senderUser.balance,
      sender_balance_after: this.senderUser.balance - (this.amountToSend + this.sendingFee),
      sender_fees: this.sendingFee,
      receiver_balance_before: null,
      receiver_balance_after: null,
      receiver_fees: null,
      type: 'p2p'
    };
    try {
      this.transaction = await CrudService.create(Transaction,{},newTransaction);
    } catch (error) {
        throw error;
    }

    const new_day_sent_count = this.senderUser.day_sent_count+1;

    let senderNewData = {
      balance: this.senderUser.balance - (this.amountToSend + this.sendingFee),
      day_sent: this.senderUser.day_sent + (this.amountToSend + this.sendingFee),
      month_sent: this.senderUser.month_sent + (this.amountToSend + this.sendingFee),
      day_sent_count: new_day_sent_count
    };

    try {
      await CrudService.update(User,this.senderUser.id, senderNewData);
      return this.transaction.id;
    } catch (error) {
      await CrudService.delete(Transaction,this.transaction.id);
      throw error;
    }

  }

  async validateSending() {
    if(this.senderUser.balance < this.amountToSend){
      throw "insuffient amount. Transaction Refused";
    }
    //gettng the sender limits and fees
    try {
      this.senderLimit = await CrudService.getById(Limit,this.senderUser.limit_id);
      this.senderFees = await CrudService.getById(Fees,this.senderUser.fees_id);
    } catch (error) {
      throw error;
    }

    //calculating the sending fees
    var sendingFee:number = 0;
    if(this.senderFees.s_fees_type == "flat"){
       this.sendingFee = this.senderFees.sender_fees;
    }else{
       this.sendingFee = this.senderFees.sender_fees/100*this.amountToSend;
    }

    //check if the user exceeded the daily amount he can send
    if(this.senderUser.day_sent + this.amountToSend + sendingFee > this.senderLimit.daily_send_amount){
      throw "you have exceeded the daily amount to send. Transaction Refused";
    }
    //check if the user exceeded the number of daily count he can send
    if(this.senderUser.day_sent_count+1 > this.senderLimit.daily_send_count){
       throw "you have exceeded the daily count to send. Transaction Refused";
    }

  }

  async receive() {

    try {
      await this.validateReceiving();
    } catch (error) {
      throw error;
    }
    const new_day_received_count = this.receiverUser.day_received_count+1;

    let receiverNewDate = {
      balance: this.receiverUser.balance + this.transaction.amount - this.receivingFee,
      day_received: this.receiverUser.day_received + (this.transaction.amount - this.receivingFee),
      month_receive: this.receiverUser.month_receive + (this.transaction.amount - this.receivingFee),
      day_received_count : new_day_received_count
    };

    let newTransactionData = {
      receiver_balance_before: this.receiverUser.balance,
      receiver_balance_after: this.receiverUser.balance + (this.transaction.amount - this.receivingFee),
      receiver_fees: this.receivingFee
    }

    try {
      await CrudService.update(User,this.receiverUser.id, receiverNewDate);
      await CrudService.update(Transaction,this.transaction.id, newTransactionData);
      return {res: "transaction completed"};
    } catch (error) {
      await CrudService.delete(Transaction,this.transaction.id);
      throw error;
    }
  }

  async validateReceiving() {
    //get all receiver data from db
    try {
      this.receiverLimit = await CrudService.getById(Limit,this.receiverUser.limit_id);
      this.receiverFees = await CrudService.getById(Fees, this.receiverUser.fees_id);
    } catch (error) {
      res.status(error.status).json(error.error);
      return;
    }

    //calculating the receiving fees
    if(this.receiverFees.s_fees_type == "flat"){
       this.receivingFee = this.receiverFees.receive_fees;
    }else{
       this.receivingFee = this.receiverFees.receive_fees/100*this.amountToSend;
    }

    //check if the receiver has exceeded the daily amount he can receive
    if(this.receiverUser.day_received+ this.amountToSend + this.receivingFee >= this.receiverLimit.daily_receive_amount){
      throw "the receiver have exceeded the daily amount to receive. Transaction Refused";
    }

    //check if the receiver exceeded the number of daily count he can receive
    if(this.receiverUser.day_received_count +1 > this.receiverLimit.daily_receive_count){
      throw "the receiver have exceeded the daily count to receive. Transaction Refused";
    }

  }
}
