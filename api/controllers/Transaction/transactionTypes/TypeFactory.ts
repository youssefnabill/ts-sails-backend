import { p2p } from "./p2p";
import {DefaultType} from "./DefaultType";

export class TypeFactory{
  getType(typeName:string):any {
    if(typeName == 'p2p'){
      return new p2p();
    }else{
      return new DefaultType();
    }
  }
}
