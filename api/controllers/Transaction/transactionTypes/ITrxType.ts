
export interface ITrxType{

  send :(senderId:number, receiverId:number, amountToSend:number) => any;
  receive:(receiverId:number, transaction:any) => any;
}


// senderUser:any;
// receiverUser:any;
// amountToSend:number;
// transaction:any;

//init: (senderUser:any, receiverUser:any, amountToSend:number, transaction:any) => any
