export class TransactionLogic{

    async send(senderId:number, receiverId:number, amountToSend:number){
      try {
        let senderUser = await this.getSenderUser(senderId);
        let receiverUser = await this.getReceiverUser(receiverId);
        await this.suffientAmountcheck(senderUser, amountToSend);
        let senderLimits = await this.getSenderLimits(senderUser);
        let senderFees = await this.getSenderFees(senderUser);
        let sendingFee = await this.calculateSendingFee(senderFees, amountToSend);
        await this.checkSenderLimits(senderUser, amountToSend, sendingFee, senderLimits);
        let trx = await this.makeNewTransactionRow(senderUser, receiverUser, amountToSend, sendingFee, 'p2p');
        await this.updateSenderInfo(senderUser,amountToSend, sendingFee,);
        return trx;
      } catch (error) {
        throw error;
      }
    }


    async receive(receiverId:number, transaction:any){
      try {
        let receiverUser = await this.getReceiverUser(receiverId);
        let receiverLimits =  await this.getReceiverLimits(receiverUser);
        let receiverFees = await this.getReceiverFees(receiverUser);
        let receivingFee = await this.calculateReceivingFee(receiverFees, transaction.amount);
        await this.checkReceiverLimits(receiverUser,transaction.amount, receivingFee, receiverLimits);
        await this.updatingReceiverData(receiverUser, transaction.amount, receivingFee);
        await this.updatingTransactionDataUponReceiving(transaction,receiverUser,receivingFee);
      } catch (error) {
        throw error;
      }

    }
    async getSenderUser(senderId:number){
      let senderUser: User;
      try {
        senderUser = await CrudService.getById(User,senderId);
        if(senderUser === null){
          throw 'User not found';
        }
        return senderUser;
      } catch (error) {
        throw error;
      }
    }
     async suffientAmountcheck(senderUser:any, amountToSend:number){
      if(senderUser.balance < amountToSend){
        throw 'insuffient amount to send'
      }
     }


     async getSenderLimits(senderUser:any){
      try {
        var senderLimit:Limit = await CrudService.getById(Limit,senderUser.limit_id);
        return senderLimit;
      } catch (error) {
        throw error;
      }
     }

     async getSenderFees(senderUser:any){
      try {
        var senderFees:Fees = await CrudService.getById(Fees,senderUser.fees_id);
        return senderFees;
      } catch (error) {
        throw error;
      }
     }

     async calculateSendingFee(senderFees:any, amountToSend:number){
      var sendingFee:number = 0;
      if(senderFees.s_fees_type == "flat"){
         return sendingFee = senderFees.sender_fees;
      }else{
         return sendingFee = senderFees.sender_fees/100*amountToSend;
      }
     }

     async checkSenderLimits(senderUser:any, amountToSend:number, sendingFee:number, senderLimit:any){
      if(senderUser.day_sent + amountToSend + sendingFee > senderLimit.daily_send_amount){
        throw 'you have exceeded the daily amount to send. Transaction Refused';
      }

      if(senderUser.day_sent_count+1 > senderLimit.daily_send_count){
        throw 'you have exceeded the number of allowed sending requests. Transaction Refused';
      }
     }

     async makeNewTransactionRow(senderUser:any, receiverUser:any, amountToSend:number, sendingFee:number, typeOfTransaction:string){
      let newTransaction:Transaction = {
        amount: amountToSend,
        date: null,
        sender_id: senderUser.id,
        receiver_id: receiverUser.id,
        sender_balance_before: senderUser.balance,
        sender_balance_after: senderUser.balance - (amountToSend + sendingFee),
        sender_fees: sendingFee,
        receiver_balance_before: null,
        receiver_balance_after: null,
        receiver_fees: null,
        type: typeOfTransaction
      };
      try {
        var trx = await CrudService.create(Transaction,{},newTransaction);
        return trx;
      } catch (error) {
        throw error;
      }
     }

     async updateSenderInfo(senderUser:any, amountToSend:number, sendingFee:number){

      const new_day_sent_count = senderUser.day_sent_count+1;
      let senderNewData = {
        balance: senderUser.balance - (amountToSend + sendingFee),
        day_sent: senderUser.day_sent + (amountToSend + sendingFee),
        month_sent: senderUser.month_sent + (amountToSend + sendingFee),
        day_sent_count: new_day_sent_count
      };
      try {
        await CrudService.update(User,senderUser.id, senderNewData);
        return true;
      } catch (error) {
        throw error;
      }
     }
     async getReceiverUser(receiverId:number){
      try {
        var receiverUser:User = await CrudService.getById(User,receiverId);
        return receiverUser;
      } catch (error) {
        throw error;
      }
     }

     async getReceiverLimits(receiverUser:any){
      try {
        var receiverLimit:Limit = await CrudService.getById(Limit,receiverUser.limit_id);
        return receiverLimit;
      } catch (error) {
        throw error;
      }
     }

     async getReceiverFees(receiverUser:any){
      try {
        var receiveFees:Fees = await CrudService.getById(Fees, receiverUser.fees_id);
        return receiveFees;
      } catch (error) {
        throw error;
      }
     }


     async calculateReceivingFee(receiveFees:any, amountToSend:number){
      var receivingFee = 0;
      if(receiveFees.s_fees_type == "flat"){
         receivingFee = receiveFees.receive_fees;
         return receivingFee;
      }else{
        receivingFee = receiveFees.receive_fees/100*amountToSend;
        return receivingFee;
     }
    }

    async checkReceiverLimits(receiverUser:any, amountToSend:number, receivingFee:number, receiverLimit:any){
      if(receiverUser.day_received+ amountToSend +receivingFee >= receiverLimit.daily_receive_amount){
        throw 'the receiver have exceeded the daily amount to receive. Transaction Refused';
      }

      if(receiverUser.day_received_count +1 > receiverLimit.daily_receive_count){
        throw 'the receiver have exceeded the daily count to receive. Transaction Refused';
      }

     }

     async updatingReceiverData(receiverUser:any, amountToSend:number, receivingFee:number){
      const new_day_received_count = receiverUser.day_received_count+1;
      let receiverNewDate = {
        balance: receiverUser.balance + amountToSend - receivingFee,
        day_received: receiverUser.day_received + (amountToSend - receivingFee),
        month_receive: receiverUser.month_receive + (amountToSend - receivingFee),
        day_received_count : new_day_received_count
      };
      try {
        await CrudService.update(User,receiverUser.id, receiverNewDate);
        return true;
      } catch (error) {
        throw error;
      }
     }



     async updatingTransactionDataUponReceiving(transaction:any, receiverUser:any, receivingFee:number){
      let newTransactionData:any = {
        receiver_balance_before: receiverUser.balance,
        receiver_balance_after: receiverUser.balance + (transaction.amount - receivingFee),
        receiver_fees: receivingFee,
      };
      try {
        await CrudService.update(Transaction,transaction.id, newTransactionData);
        return true;
      } catch (error) {
        throw error;
      }
     }

}
