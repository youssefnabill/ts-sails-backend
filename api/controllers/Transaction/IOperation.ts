
export interface IOperation{
    senderId:number;
    amountToSend:number;
    receiverId:number;
    execute :() => void
}
