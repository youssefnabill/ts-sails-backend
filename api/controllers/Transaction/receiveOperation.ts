import { IOperation } from "./IOperation";
import { TypeFactory } from "./transactionTypes/TypeFactory";
import { ITrxType } from "./transactionTypes/ITrxType";

export class receiveOperation implements IOperation{
    senderId:number;
    receiverId:number;
    amountToSend:number;
    transactionId:number;

    constructor(receiverId:number, transactionId:number){
      this.receiverId = receiverId;
      this.transactionId = transactionId;
    }
    async execute () {
      try {
        var receiverUser:User = await CrudService.getById(User,this.receiverId);
        var transaction:Transaction = await CrudService.getById(Transaction, this.transactionId);
        if(receiverUser == null){
          throw 'receiver not found';
        }
        if(transaction == null){
          throw 'transaction not found';
        }
        let tf = new TypeFactory();
        let trx: ITrxType = tf.getType(transaction.type);
        await trx.receive(receiverUser.id, transaction);
      } catch (error) {
        throw error;
      }
    }
}
