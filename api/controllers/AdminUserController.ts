import * as bcrypt from 'bcrypt';
const jwt = require('jsonwebtoken');
const { validateAll } = require('indicative');
class AdminUserController {
  //Create new admin user
  async create(req: Request, res: Response) {
    const rules = {
      email: 'required|email',
      password: 'required',
      name: 'required',
      username: 'required'
    };
    let adminUser:AdminUser;
    let hash = await bcrypt.hash(req.body.password, 10);
      adminUser = {
        id: null,
        username: req.body.username,
        name: req.body.name,
        email: req.body.email,
        password: hash,
        is_super: null
      };
      try{
        var result = await CrudService.create(AdminUser, rules,adminUser);
        res.status(200).json(result);
      }catch(error){
        res.status(error.status).json({error: error.error});
      }
  }

  async getById(req: Request, res: Response) {
    try{
      var result = await CrudService.getById(AdminUser, parseInt(req.params.id));
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).json({error: error.error});
    }
  }

  async getall(req: Request, res: Response) {
    try{
      var result = await CrudService.getall(AdminUser);
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).json({error: error.error});
    }
  }
  async update(req: Request, res: Response) {
    try {
      if(req.body.password){
        req.body.password = await bcrypt.hash(req.body.password,10);
      }
      let result = await CrudService.update(AdminUser,req.params.id,req.body);
      res.status(200).json({newRow : result});
    } catch (error) {
      res.status(error.status).json({error:error});
    }
  }
  async delete(req: Request, res: Response) {
    try{
      var result = await CrudService.delete(AdminUser, parseInt(req.params.id));
      res.status(200).json(result);
    }catch(error){
      res.status(error.status).json({error: error.error});
    }
  }

  async login(req:Request, res:Response){
    let email = req.body.email;
    let password = req.body.password;
    //setting the rules of validation
    const rules = {
      email: 'required|email',
      password: 'required|string',
    };

    //validating data from the request
    try{
      await validateAll({
        email : email,
        password: password
      },rules);
    }catch(error){
      res.status(500).json({error: error});
    }
    //getting the user by email
    try {
      let adminUser = await AdminUser.findOne({
        where: {
          email : email
        }
      });
      //comparing user password with the input password
      let result = await bcrypt.compare(password,adminUser.password);
      if(result){
        //password match and sending jwt in response
        var token = jwt.sign({
          id: adminUser.id,
          isAdmin: true,
          isSuper: adminUser.is_super
        },'secret');
        res.status(200).json({
          token: token,
          id: adminUser.id,
          isAdmin: true,
          isSuper: adminUser.is_super
        });
      }else{
        res.status(500).json({error:"password doesn't match"});
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({error: error});
    }
  }
}

module.exports = new AdminUserController();
