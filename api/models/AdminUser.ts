import { sequelizeModel } from 'ts-sequelize-models';
import { DefineAttributes, DataTypes, Model, DefineOptions } from 'sequelize';

export class AdminUser extends sequelizeModel {

    getAttributes(DataTypes: DataTypes): DefineAttributes {
        return {
            username:{type:DataTypes.STRING,unique:true,allowNull:false},
            name: {type: DataTypes.STRING, allowNull:false},
            email: { type: DataTypes.STRING, unique: true,allowNull:false },
            password: {type: DataTypes.STRING, allowNull:false},
            is_super: {type: DataTypes.BOOLEAN, unique: true ,allowNull: true}
        };
    }
}
