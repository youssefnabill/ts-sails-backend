import { sequelizeModel } from 'ts-sequelize-models';
import { DefineAttributes, DataTypes, Model, DefineOptions } from 'sequelize';

export class User extends sequelizeModel {


    getAttributes(DataTypes: DataTypes): DefineAttributes {
        return {
          username: { type:DataTypes.STRING,unique:true,allowNull:false},
          name: {type: DataTypes.STRING, allowNull:false},
          email: { type: DataTypes.STRING, unique: true,allowNull:false },
          password: {type: DataTypes.STRING, allowNull:false},
          phone_number: {type: DataTypes.STRING, allowNull:false},
          balance: DataTypes.DOUBLE,
          limit_id: {
            type: DataTypes.INTEGER,
            references: {
              model: "Limits",key: "id"
            },
          },
          fees_id: {
            type: DataTypes.INTEGER,
            references: {
              model: "Fees",key: "id"
            },
          },
          type: {type: DataTypes.STRING, allowNull:false},
          day_sent: DataTypes.INTEGER,
          day_sent_count: DataTypes.INTEGER,
          day_received: DataTypes.INTEGER,
          day_received_count: DataTypes.INTEGER,
          month_sent: DataTypes.INTEGER,
          month_receive: DataTypes.INTEGER
        };
    }
}


