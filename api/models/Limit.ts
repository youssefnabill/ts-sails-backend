import { sequelizeModel } from 'ts-sequelize-models';
import { DefineAttributes, DataTypes, Model, DefineOptions } from 'sequelize';

export class Limit extends sequelizeModel {

    getAttributes(DataTypes: DataTypes): DefineAttributes {
        return {
          profile_name: DataTypes.STRING,
          daily_send_amount: DataTypes.INTEGER,
          daily_receive_amount: DataTypes.INTEGER,
          daily_send_count: DataTypes.INTEGER,
          daily_receive_count: DataTypes.INTEGER,
        };
    }
}
