import { sequelizeModel } from 'ts-sequelize-models';
import { DefineAttributes, DataTypes, Model, DefineOptions } from 'sequelize';

export class Transaction extends sequelizeModel {

      associate(models: any, Transaction: Model<any, any>): any {
        Transaction.belongsTo(models.User,{as: 'sender',foreignKey:'sender_id'});
        Transaction.belongsTo(models.User,{as: 'receiver',foreignKey:'receiver_id'});
        // models.User.hasMany(Transaction, { foreignKey: 'sender_id' , constraints: false});
        // models.Transaction.hasOne(models.User, { as: 'sender', foreignKey: 'sender_id' , constraints: false});
        // User.belongsTo(this);
        // you can do as many associations as you want here
      }
    getAttributes(DataTypes: DataTypes): DefineAttributes {
        return {
          amount: {type: DataTypes.DOUBLE, allowNull:false},
          sender_id: {
            type: DataTypes.INTEGER,
            references: {
              model: "Users",key: "id"
            },
            allowNull: false
          },
          receiver_id:  {
            type: DataTypes.INTEGER,
            references: {
              model: "Users",key: "id"
            },
            allowNull:false
          },
          type:{type: DataTypes.STRING},
          sender_balance_before: DataTypes.DOUBLE,
          sender_balance_after: DataTypes.DOUBLE,
          receiver_balance_before: DataTypes.DOUBLE,
          receiver_balance_after: DataTypes.DOUBLE,
          sender_fees: DataTypes.DOUBLE,
          receiver_fees: DataTypes.DOUBLE,
          date: {
            type : DataTypes.DATE,
            defaultValue : new Date()
          },
        };
    }
}
