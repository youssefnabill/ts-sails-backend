import { sequelizeModel } from 'ts-sequelize-models';
import { DefineAttributes, DataTypes, Model, DefineOptions } from 'sequelize';

export class Fees extends sequelizeModel {

    getAttributes(DataTypes: DataTypes): DefineAttributes {
        return {
          profile_name : DataTypes.STRING,
          sender_fees : DataTypes.INTEGER,
          s_fees_type: DataTypes.ENUM("percentage","flat"),
          receive_fees : DataTypes.INTEGER,
          r_fees_type: DataTypes.ENUM("percentage","flat"),
        };
    }
}
