/**
 * sessionAuth
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!/documentation/concepts/Policies
 *
 */
const jwt = require('jsonwebtoken');
 module.exports = function (req: Request, res: Response, next: Function) {

  var token = req.headers.authorization;
  try {
    jwt.verify(token, 'secret');
    return next();
  } catch (error) {
    res.status(403).send('forbidden');
  }
};
