/**
 * CrudService Service
 *
 */
const { validateAll } = require('indicative');
class Crudservice {

  async create(model:any, validationRules:Object, objectInstence:Object) {
    const rules = validationRules;
    //validating input
    try{
      await validateAll(objectInstence, rules);
    }catch(e){
      console.log(e);
      ;
      throw {
        'error' : e,
        'status': 400
      };
    }
    //validation is passed adding the model to db
    try {
      model = await model.create(objectInstence);
      return model;
    } catch (e) {
      throw {
        'error':e,
        'status':500
      }
    }

  }
  async getall(model:any) {
    try {
      var result = await model.findAll();
      return result;
    } catch (error) {
      throw{
        'error': error,
        'status':500
      }
    }
  }

  async getById(model:any, id:number) {
    try {
      const rules:Object = {
        id : 'required|number'
      };
      await validateAll(id, rules);
    } catch (error) {
      throw{
        'error': error,
        'status': 400
      }
    }
    try {
      let result = await model.findById(id);
      return result;
    } catch (error) {
      throw{
        'error': error,
        'status': 500
      }
    }
  }

  async update(model:any, modelId:any, newData:any) {
    try {
      let m = await model.findById(modelId);
      return m.update(newData);
    } catch (error) {
      throw{
        'error': error,
        'status':500
      }
    }
  }
  async delete(model:any,id:number) {
    try {
      const rules:Object = {
        id : 'required|number'
      }
      await validateAll(id, rules);
    } catch (error) {
      throw{
        'error': error,
        'status':400
      }
    }
    let isDeleted:number;
    try {
      isDeleted = await model.destroy({
        where: {
          id : id
        }
      });
      return isDeleted;
    } catch (error) {
      throw {
        'error': error,
        'status': 500
      }
    }
  }
}

module.exports = new Crudservice();
