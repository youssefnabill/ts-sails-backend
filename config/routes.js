module.exports.routes = {

  //admin user routes
  'post /admin/create' : 'AdminUserController.create',
  'get /admin/getall'  : 'AdminUserController.getall',
  'get /admin/get/:id' : 'AdminUserController.getById',
  'put /admin/update/:id' : 'AdminUserController.update',
  'delete /admin/delete/:id' : 'AdminUserController.delete',
  'post /admin/login'  : 'AdminUserController.login',

  //user routes
  'post /user/create' : 'UserController.create',
  'get /user/getall'  : 'UserController.getall',
  'get /user/get/:id' : 'UserController.getById',
  'put /user/update/:id' : 'UserController.update',
  'delete /user/delete/:id' : 'UserController.delete',
  'post /user/login'  : 'UserController.login',
  'post /user/find/email' : 'UserController.getUserByEmail',

  //fees routes
  'post /fees/create' : 'FeesController.create',
  'get /fees/getall'  : 'FeesController.getall',
  'get /fees/get/:id' : 'FeesController.getById',
  'put /fees/update/:id' : 'FeesController.update',
  'delete /fees/delete/:id' : 'FeesController.delete',

  //limit routes
  'post /limit/create' : 'LimitController.create',
  'get /limit/getall'  : 'LimitController.getall',
  'get /limit/get/:id' : 'LimitController.getById',
  'put /limit/update/:id' : 'LimitController.update',
  'delete /limit/delete/:id' : 'LimitController.delete',

  //transaction routes
  'post /trx/make' : 'TransactionController.makeTransaction',
  'get /trx/user/:id' : 'TransactionController.getTransactionsByUser',
  'get /trx/get/:id' : 'TransactionController.getById',
  'get /trx/getall' : 'TransactionController.getAllTransactions',
  'post /trx/test/' : 'TransactionController.test',

};
